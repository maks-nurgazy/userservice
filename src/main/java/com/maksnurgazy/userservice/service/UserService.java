package com.maksnurgazy.userservice.service;

import com.maksnurgazy.userservice.dto.UserDto;
import com.maksnurgazy.userservice.dto.UserInputDto;

import java.util.List;

public interface UserService {
    List<UserDto> getAllUsers();

    UserDto getUserById(Long id);

    UserDto addUser(UserInputDto userInputDto);

    UserDto updateUser(Long id, UserDto userDto);

    void deleteUser(Long id);
}
