package com.maksnurgazy.userservice.service.impl;

import com.maksnurgazy.userservice.dto.UserDto;
import com.maksnurgazy.userservice.dto.UserInputDto;
import com.maksnurgazy.userservice.entity.User;
import com.maksnurgazy.userservice.exception.ResourceNotFoundException;
import com.maksnurgazy.userservice.mapper.UserMapper;
import com.maksnurgazy.userservice.repository.UserRepository;
import com.maksnurgazy.userservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream().map(userMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public UserDto getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found"));

        return userMapper.toDto(user);
    }

    @Override
    public UserDto addUser(UserInputDto userInputDto) {
        User user = userMapper.toEntity(userInputDto);
        user.setPassword(passwordEncoder.encode(userInputDto.getPassword()));
        user.setRole("ROLE_" + userInputDto.getRole().toUpperCase());
        User savedTask = userRepository.save(user);

        return userMapper.toDto(savedTask);
    }

    @Override
    public UserDto updateUser(Long id, UserDto userDto) {
        User task = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        User taskToUpdate = userMapper.partialUpdate(userDto, task);
        User updatedTask = userRepository.save(taskToUpdate);

        return userMapper.toDto(updatedTask);
    }

    @Override
    public void deleteUser(Long id) {
        User task = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        userRepository.delete(task);
    }
}
