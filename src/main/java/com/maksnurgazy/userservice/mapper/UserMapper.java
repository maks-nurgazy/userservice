package com.maksnurgazy.userservice.mapper;

import com.maksnurgazy.userservice.config.MapstructMappingConfig;
import com.maksnurgazy.userservice.dto.UserDto;
import com.maksnurgazy.userservice.dto.UserInputDto;
import com.maksnurgazy.userservice.entity.User;
import org.mapstruct.*;

@Mapper(config = MapstructMappingConfig.class)
public interface UserMapper {

    @Mapping(target = "password", ignore = true)
    @Mapping(target = "id", ignore = true)
    User toEntity(UserInputDto userInputDto);

    UserDto toDto(User task);

    @Mapping(target = "password", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    User partialUpdate(UserDto taskDto, @MappingTarget User task);
}
