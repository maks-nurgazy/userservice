package com.maksnurgazy.userservice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class UserInputDto {
    @NotBlank(message = "Name cannot be blank")
    @Size(max = 255, message = "Name length must be less than or equal to 255 characters")
    private String name;

    @NotBlank(message = "Email cannot be blank")
    @Size(max = 255, message = "Email length must be less than or equal to 255 characters")
    private String email;

    @NotBlank(message = "PhoneNumber cannot be blank")
    @Size(max = 255, message = "PhoneNumber length must be less than or equal to 255 characters")
    private String phoneNumber;

    @NotBlank(message = "Password cannot be blank")
    @Size(max = 255, message = "Password length must be less than or equal to 255 characters")
    private String password;

    @NotBlank(message = "Role cannot be blank")
    @Size(max = 20, message = "Role length must be less than or equal to 255 characters")
    private String role;
}
