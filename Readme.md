# UserService

A Spring Boot application for managing users with CRUD operations and simple authentication/authorization.

## Features

- CRUD operations for users
- Basic authentication and authorization using Spring Security

## How to Run

1. Make sure you have Java 17 and Maven installed on your machine.
2. Clone the repository
3. Navigate to the project directory.
4.  ```bash
    cd UserService
    ```

5. Build the application using Maven:

    ```bash
    mvn clean package
    ```

6. Run the application with the 'dev' profile active:

    ```bash
    java -jar -Dspring.profiles.active=dev target/userservice.jar
    ```

The application will be available at `http://localhost:8080`.

## API Endpoints

- `GET /users` - Get all users
- `GET /users/{id}` - Get user by ID
- `POST /users` - Create a new user
- `PUT /users/{id}` - Update an existing user
- `DELETE /users/{id}` - Delete a user

## Default User

- Email: `jack@example.com`
- Password: `maks`

## Swagger url: `http://localhost:8080/userservice/api/v1/swagger-ui/index.html#`
